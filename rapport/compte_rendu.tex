\documentclass{article}
\usepackage[utf8]{inputenc}
\usepackage{graphicx}
\usepackage{enumitem}

\title{Compte rendu de Projet \\ Introduction au développement sous Android}
\author{
    Dumont Quentin \\
    \texttt{quentin1.dumont@gmail.com} \\
    M2 Web et Science de Données \\
    Université du Littoral Côte d’Opale \\
    Calais
}
\date{19 janvier 2024}

\begin{document}

\maketitle

\tableofcontents

\section{Introduction}
L'objectif de ce projet est de développer une application Android permettant à l'utilisateur de répondre à des questionnaires à choix multiples (QCM) intégrés dans l'application et de suivre ses progrès. De plus, l'application devrait offrir la possibilité de créer de nouveaux questionnaires via une interface d'administration. Le compte rendu qui suit détaille les différentes phases de développement de l'application, les décisions techniques prises et les défis rencontrés.

\section{Présentation de l’application}
L’application est composée de 4 activités :
\begin{enumerate}
    \item \texttt{MainActivity} : activité principale de l’application, elle permet à l’utilisateur de répondre à différents questionnaires, de consulter ses scores, etc.
    \item \texttt{FormActivity} : activité permettant à l’utilisateur de répondre au questionnaire sélectionné.
    \item \texttt{ScoresActivity} : activité permettant à l’utilisateur de consulter ses scores.
    \item \texttt{CreateFormActivity} : activité permettant à l’utilisateur de créer de nouveaux questionnaires.
\end{enumerate}

Les questionnaires sont stockés dans un fichier JSON avec le format suivant : le nom associé à l’objet JSON représentant un questionnaire contenant \texttt{"theme"} associé au thème du questionnaire et \texttt{"questions"} associé à un tableau d’objets JSON représentants les questions. Ces dernières contiennent \texttt{"question"} associé à l’intitulé de la question, \texttt{"choix"} associé à un tableau des différents choix de réponse, et \texttt{"reponse"} associé à l’indice de la bonne réponse.

Lorsqu’un questionnaire est sélectionné, l’utilisateur peut voir le thème du questionnaire,  sa progression dans les questionnaire et répondre aux questions qui lui sont posées dans un ordre aléatoire. L’utilisateur peut choisir d’abandonner le questionnaire et obtiendra alors un score de 0.

Après avoir abandonné ou répondu à toute les questions le score du questionnaire sera affiché et l’utilisateur peut choisir soit de retourner au menu de sélection ou de consulter ses scores. Ces derniers, stockés dans un fichier JSON seront alors affiché ainsi qu’une moyenne des scores ramenée sur 20. Les scores peuvent également être consultés depuis le menu d’option de l’activité principale.

Après avoir obtenu un score à un questionnaire, ce dernier ne sera plus disponible. Il est toutefois possible de réinitialiser les scores via le menu d’option de l’activité principale ce qui rends tous les questionnaires disponibles.

Il est possible de créer un nouveau questionnaire en passant par le menu d’option de l’activité principale. Il faut rentrer le nom et la catégorie du questionnaire ainsi que les différentes questions qui seront validées une par une, une fois toutes les questions validées, l’utilisateur peut valider le questionnaire avant d'être redirigé vers le menu de sélection.

L’application est disponible en français avec l’anglais comme langue par défaut, les questionnaires disponibles sont aussi affectés par la langue de l’application.

\begin{figure}[h]
    \centering
    \begin{minipage}{0.3\textwidth}
        \includegraphics[width=\linewidth]{figure1a.jpg}
    \end{minipage}
    \begin{minipage}{0.3\textwidth}
        \includegraphics[width=\linewidth]{figure1b.jpg}
    \end{minipage}
    \begin{minipage}{0.3\textwidth}
        \includegraphics[width=\linewidth]{figure1c.jpg}
    \end{minipage}
    \caption{Activités principales}
\end{figure}

\newpage

\section{Choix techniques}

\subsection{Représentation et stockage des questionnaires et des scores}
Les questionnaires sont stockés dans le fichier forms.json ou form-fr.json si l’application est en français, ils sont représentés par la classe Form qui contient la catégorie et la liste des questions. Ces dernières sont représentés par la classe Question qui comporte l’intitulé de la question, les choix de réponse ainsi que l’indice de la bonne réponse. Ces deux classe ont une méthode toJSON pour la sérialisation et une méthode fromJSON pour désérialiser, permettant respectivement la sauvegarde et le chargement des questionnaires.

Les scores sont stockés dans le fichier scores.json ou scores-fr.json si l’application est en français, ils sont représentés par des chaînes de caractères de la forme \texttt{nombre de bonne réponse} / \texttt{nombre de questions} facilitant leur sauvegarde et leur chargement.

Les questionnaires et les scores sont chargés dans des HashMap, utilisant le nom attribué au questionnaire comme clé, au lancement de l’application et sont sauvegardés depuis celles ci à la fermeture de l’application. Ces HashMap sont des variables de classes de MainActivity, permettant aux différentes Activité d’interagir avec.

\subsection{Gestion dynamique des questionnaires et des scores}
Afin d’afficher dynamiquement la liste des questionnaires disponibles dans l’activité principale, une ListView est remplie avec les nom des questionnaires qui n’ont pas déjà été effectués auxquels sont appliqué la méthode setOnclickListener pour lancer l’activité FormActivity en passant le nom du questionnaire.

L’activité FormActivity récupère le questionnaire correspondant dans la HashMap forms, et effectue une copie des questions qui sera ensuite mélangée par la méthode Collections.shuffle permettant d’avoir un ordre aléatoire. Les choix de questions sont affichés dans une ListView à choix unique, ils sont rafraîchis ainsi que l’intitulé de la question et la progression à chaque fois qu’une réponse est validée.

Une fois le questionnaire terminé, le score du questionnaire est affiché et stocké dans la HashMap scores. Les boutons stopper et valider deviennent respectivement des boutons permettant de retourner au menu de sélection et d’afficher les scores.

L’activité ScoresActivity affiche les scores dans une ListView utilisant un adapter spécialement conçu pour s’adapter à la HashMap. La moyenne est également ajoutée à la ListView, elle est calculée dynamiquement et s’affiche selon la langue du système.



\subsection{Partie administrateur}

Lorsque l’utilisateur tente de créer un questionnaire, une boite de dialogue demandant le mot de passe administrateur est affichée, si il est correcte l’utilisateur accédera à la création de questionnaire.

Lorsque l’utilisateur clique sur Valider la question, on vérifie si la question et les choix de réponse ne sont pas vide et si l’un des choix a été défini comme bonne réponse, si c’est le cas la question est ajoutée à un ArrayList et les champs concernés sont vidés, dans le cas contraire un message d’erreur est affiché.

Lorsque l’utilisateur clique sur Valider le QCM, on vérifie que le nom et la catégorie ne sont pas vide et qu’au moins une question a été validée, si c’est le cas le questionnaire est ajouté à la HashMap et on reviens sur l’activité principale, dans le cas contraire un message d’erreur est affiché.

\begin{figure}[h]
    \centering
    \begin{minipage}{0.25\textwidth}
        \includegraphics[width=\linewidth]{figure2a.jpg}
    \end{minipage}
    \begin{minipage}{0.25\textwidth}
        \includegraphics[width=\linewidth]{figure2b.jpg}
    \end{minipage}
    \begin{minipage}{0.4\textwidth}
        \includegraphics[width=1.3\linewidth]{figure2c.jpg}
    \end{minipage}
    \caption{Partie administrateur}
\end{figure}

\newpage

\section{Conclusion}
Ce projet a offert l'opportunité de mettre en pratique les compétences acquises lors des cours de développement Android. Les défis rencontrés ont été stimulants à résoudre, contribuant ainsi à approfondir les connaissances en développement Android et en Java, notamment en ce qui concerne la gestion dynamique des questionnaires et la sérialisation/désérialisation des données.

L'application est actuellement finalisée et opérationnelle, mais des possibilités d'amélioration subsistent. Parmi celles-ci, on peut citer la nécessité d'intégrer des fonctionnalités telles que la modification du mot de passe d'administration ou le renforcement de la sécurité du stockage de ce dernier. D'autres améliorations potentielles pourraient inclure la possibilité de supprimer des questionnaires, de modifier les questionnaires existants, ou d'afficher les scores par catégorie avec une moyenne par catégorie et une moyenne globale.

\end{document}
