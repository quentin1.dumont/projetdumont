package com.example.projetdumont;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Adapter pour la liste des scores
 */
public class ScoreAdapter extends BaseAdapter {

    Context context;
    HashMap<String, String> scores;
    private ArrayList<String> keys;

    public ScoreAdapter(Context context, HashMap<String, String> scores) {
        super();
        this.context = context;
        this.scores = scores;
        keys = new ArrayList<>(scores.keySet());
    }

    @Override
    public int getCount() {
        return scores.size();
    }

    @Override
    public Object getItem(int i) {
        return new ScoreItem(keys.get(i), scores.get(keys.get(i)));
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    /**
     * Récupère la vue d'un item de la liste des scores
     * @param i index de l'item
     * @param view vue de l'item
     * @param viewGroup vue du groupe
     * @return vue de l'item
     */
    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder holder;
        if (view == null) {
            view = View.inflate(context, R.layout.score, null);
            holder = new ViewHolder();
            holder.name = view.findViewById(R.id.name);
            holder.score = view.findViewById(R.id.score);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        ScoreItem scoreItem = (ScoreItem) getItem(i);
        holder.name.setText(scoreItem.getName());
        holder.score.setText(scoreItem.getScore());

        return view;
    }

    /**
     * Ajoute un item à la liste des scores
     * @param scoreItem item à ajouter
     */
    public void add(ScoreItem scoreItem) {
        scores.put(scoreItem.getName(), scoreItem.getScore());
        keys.add(scoreItem.getName());
    }

    /**
     * Classe représentant un item de la liste des scores
     */
    private static class ViewHolder {
        TextView name;
        TextView score;
    }

    /**
     * Classe représentant la paire nom du QCM, score
     */
    public static class ScoreItem {
        private String name;
        private String score;

        public ScoreItem(String name, String score) {
            this.name = name;
            this.score = score;
        }

        public String getName() {
            return name;
        }

        public String getScore() {
            return score;
        }
    }
}
