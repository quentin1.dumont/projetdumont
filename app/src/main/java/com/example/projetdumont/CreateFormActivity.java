package com.example.projetdumont;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Toast;

import org.json.JSONException;

import java.util.ArrayList;

public class CreateFormActivity extends AppCompatActivity {

    private EditText question;
    private EditText category;
    private EditText choice1;
    private EditText choice2;
    private EditText choice3;
    private RadioGroup radioGroup;

    private ArrayList<Question> questions;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_form);

        // Initialisation des variables
        question = findViewById(R.id.question);
        category = findViewById(R.id.category);
        choice1 = findViewById(R.id.choice1);
        choice2 = findViewById(R.id.choice2);
        choice3 = findViewById(R.id.choice3);
        radioGroup = findViewById(R.id.choices);
        questions = new ArrayList<>();
    }

    /**
     * Ajoute une question au formulaire
     * @param view bouton de validation de la question
     */
    public void submitQuestion(View view) {
        String question = this.question.getText().toString();
        if (question.isEmpty()) {
            // Si la question est vide, on affiche un message d'erreur
            Toast.makeText(this, R.string.empty_question, Toast.LENGTH_SHORT).show();
            return;
        }

        // Initialisation des choix
        String [] choices = new String[3];
        choices[0] = this.choice1.getText().toString();
        choices[1] = this.choice2.getText().toString();
        choices[2] = this.choice3.getText().toString();

        if (choices[0].isEmpty() || choices[1].isEmpty() || choices[2].isEmpty()) {
            // Si au moins un des choix est vide, on affiche un message d'erreur
            Toast.makeText(this, R.string.empty_choices, Toast.LENGTH_SHORT).show();
            return;
        } else if (choices[0].equals(choices[1]) || choices[0].equals(choices[2]) || choices[1].equals(choices[2])) {
            // Si au moins deux choix sont identiques, on affiche un message d'erreur
            Toast.makeText(this, R.string.same_choices, Toast.LENGTH_SHORT).show();
            return;
        }

        // On récupère la bonne réponse
        int checkedId = this.radioGroup.getCheckedRadioButtonId();
        int answer = 0;
        if (checkedId == -1) {
            // Si aucune réponse n'est sélectionnée, on affiche un message d'erreur
            Toast.makeText(this, R.string.no_answer, Toast.LENGTH_SHORT).show();
            return;
        }
        else if (checkedId == R.id.choice2_radio) {
            answer = 1;
        } else if (checkedId == R.id.choice3_radio) {
            answer = 2;
        }

        // On ajoute la question à la liste des questions
        Question q = new Question(question, answer, choices);
        questions.add(q);

        // On vide les champs
        this.question.setText("");
        this.choice1.setText("");
        this.choice2.setText("");
        this.choice3.setText("");
        this.radioGroup.clearCheck();
    }

    /**
     * Quitte l'activité
     * @param view bouton d'annulation
     */
    public void quit(View view) {
        finish();
    }

    /**
     * Valide le QCM et l'ajoute à la liste des QCM
     * @param view bouton de validation du QCM
     */
    public void submitForm(View view) {
        String name = ((EditText) findViewById(R.id.name)).getText().toString();
        if (name.isEmpty()) {
            // Si le nom est vide, on affiche un message d'erreur
            Toast.makeText(this, R.string.empty_name, Toast.LENGTH_SHORT).show();
            return;
        } else if (MainActivity.forms.containsKey(name)) {
            // Si le nom existe déjà, on affiche un message d'erreur
            Toast.makeText(this, R.string.name_exists, Toast.LENGTH_SHORT).show();
            return;

        }

        String category = this.category.getText().toString();
        if (category.isEmpty()) {
            // Si la catégorie est vide, on affiche un message d'erreur
            Toast.makeText(this, R.string.empty_category, Toast.LENGTH_SHORT).show();
            return;
        }

        if (questions.isEmpty()) {
            // Si aucune question n'a été ajoutée, on affiche un message d'erreur
            Toast.makeText(this, R.string.no_question, Toast.LENGTH_SHORT).show();
            return;
        }

        // On crée le QCM et on l'ajoute à la liste des QCM avant de quitter l'activité
        Form form = new Form(category, questions);
        MainActivity.forms.put(name, form);
        System.out.println(MainActivity.forms.size());
        finish();
    }
}