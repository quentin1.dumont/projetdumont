package com.example.projetdumont;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.LocaleList;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {
    private ArrayAdapter<String> adapter;
    private ListView listView;
    public static File formsFile;
    public static File scoresFile;

    public static HashMap<String,Form> forms;

    public static HashMap<String, String> scores;

    /**
     * Récupère la locale de l'appareil
     * @param context le contexte de l'application
     * @return la locale de l'appareil, par défaut l'anglais
     */
    public static Locale getLocale(Context context) {
        LocaleList localeList = context.getResources().getConfiguration().getLocales();
        if (localeList.size() > 0) {
            if (localeList.get(0).getLanguage().equals("fr")) {
                return Locale.FRENCH;
            } else {
                return Locale.ENGLISH;
            }
        }
        return Locale.ENGLISH;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Initialisation des scores et des QCM
        if (MainActivity.scores == null) {
            initScores();
        }
        if (forms == null) {
            initForms();
        }

        setContentView(R.layout.activity_main);
        setSupportActionBar(findViewById(R.id.title_bar));

        // Initialisation de la liste des QCM
        ArrayList<String> entries = new ArrayList<>();
        adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, entries);
        listView = findViewById(R.id.list_qcm);
        listView.setAdapter(adapter);

        loadList();
    }

    /**
     * Initialise les scores
     */
    private void initScores() {
        MainActivity.scores = new HashMap<>();
        scoresFile = new File(getFilesDir(), getResources().getString(R.string.scores_json));
        try {
            FileInputStream fis = new FileInputStream(scoresFile);
            BufferedReader br = new BufferedReader(new java.io.InputStreamReader(fis));
            StringBuilder json = new StringBuilder();

            // Lecture du fichier json
            String line;
            while ((line = br.readLine()) != null) {
                json.append(line);
            }

            // Chargement des scores depuis le fichier json
            JSONObject json_scores = new JSONObject(json.toString());
            Iterator<String> keys = json_scores.keys();
            while (keys.hasNext()) {
                String key = keys.next();
                scores.put(key, json_scores.getString(key));
            }

        } catch (FileNotFoundException e) {
            try {
                // Création du fichier json s'il n'existe pas
                scoresFile.createNewFile();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Charge la liste des QCM sans score
     */
    private void loadList() {
        adapter.clear();
        for (String name : forms.keySet()) {
            // On ne charge pas les QCM qui ont déjà un score
            if (scores.containsKey(name)) {
                continue;
            }
            adapter.add(name);
            // Lorsqu'on clique sur un QCM, on lance l'activité FormActivity avec le nom du QCM
            listView.setOnItemClickListener((parent, view, position, id) -> {
                Intent intent = new Intent(MainActivity.this, FormActivity.class);
                intent.putExtra("name", name);
                startActivity(intent);
            });
        }
        adapter.notifyDataSetChanged();
    }

    /**
     * Initialise les QCM
     */
    private void initForms() {
        formsFile = new File(getFilesDir(), getResources().getString(R.string.forms_json));
        forms = new HashMap<>();
        try {
            FileInputStream fis = new FileInputStream(formsFile);
            BufferedReader br = new BufferedReader(new java.io.InputStreamReader(fis));
            StringBuilder json = new StringBuilder();

            // Lecture du fichier json
            String line;
            while ((line = br.readLine()) != null) {
                json.append(line);
            }

            // Chargement des QCM depuis le fichier json
            JSONObject jsonForms = new JSONObject(json.toString());
            Iterator<String> keys = jsonForms.keys();
            while (keys.hasNext()) {
                String key = keys.next();
                forms.put(key, Form.fromJSON(jsonForms.getJSONObject(key)));
            }

        } catch (FileNotFoundException e) {
            try {
                // Création du fichier json s'il n'existe pas
                formsFile.createNewFile();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Gère les actions du menu
     * @param item l'item du menu cliqué
     */
    public void onClick(MenuItem item) {
        if (item.getItemId() == R.id.scores) {
            // Lorsqu'on clique sur le menu "Scores", on lance l'activité Scores
            Intent intent = new Intent(MainActivity.this, ScoresActivity.class);
            startActivity(intent);
        } else if (item.getItemId() == R.id.add_qcm) {
            // Lorsqu'on clique sur le menu "Ajouter un QCM", on demande le mot de passe via une boîte de dialogue
            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
            builder.setTitle(R.string.enter_password);
            View viewInflated = LayoutInflater.from(getContext()).inflate(R.layout.password, (ViewGroup) findViewById(R.id.content), false);
            final EditText input = (EditText) viewInflated.findViewById(R.id.input);
            builder.setView(viewInflated);

            // bouton valider
            builder.setPositiveButton(android.R.string.ok, (dialog, which) -> {
                dialog.dismiss();
                String password = input.getText().toString();
                if (password.equals(getResources().getString(R.string.pwd))) {
                    // Si le mot de passe est correct, on lance l'activité CreateFormActivity
                    Intent intent = new Intent(MainActivity.this, CreateFormActivity.class);
                    startActivity(intent);
                } else {
                    // Sinon, on affiche un message d'erreur
                    Toast.makeText(getContext(), R.string.wrong_password, Toast.LENGTH_SHORT).show();
                }
            });

            // bouton annuler, on ferme la boîte de dialogue
            builder.setNegativeButton(android.R.string.cancel, (dialog, which) -> dialog.cancel());

            builder.show();
        } else if (item.getItemId() == R.id.reset_scores) {
            // Lorsqu'on clique sur le menu "Réinitialiser les scores", on supprime les scores et on recharge la liste des QCM
            scores.clear();
            loadList();
        } else if (item.getItemId() == R.id.quit) {
            // Lorsqu'on clique sur le menu "Quitter", on quitte l'application
            finish();
        }
    }

    /**
     * Retourne le contexte de l'activité pour pouvoir l'utiliser dans la boîte de dialogue
     * @return le contexte de l'activité
     */
    private Context getContext() {
        return this;
    }

    /**
     * Sauvegarde les QCM et les scores lorsqu'on quitte l'application
     */
    @Override
    public void onStop() {
        saveForms();
        saveScores();
        super.onStop();
    }

    /**
     * Recharge la liste des QCM sans score lorsqu'on revient sur l'activité
     */
    @Override
    public void onResume() {
        super.onResume();
        loadList();
    }

    /**
     * Crée le menu
     * @param menu le menu à créer
     * @return true si le menu a été créé
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        MenuInflater inflater= getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return true;
    }

    /**
     * Sauvegarde les QCM dans un fichier json
     */
    private void saveForms() {
        JSONObject json = new JSONObject();
        for (String name : forms.keySet()) {
            try {
                json.put(name, forms.get(name).toJSON());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        saveFile(formsFile, json, "Error while saving forms");
    }

    /**
     * Écrit le json dans le fichier
     * @param file le fichier json
     * @param json le json à sauvegarder
     * @param errorMessage le message d'erreur à afficher en cas d'erreur
     */
    private static void saveFile(File file, JSONObject json, String errorMessage) {
        try {
            FileWriter writer = new FileWriter(file);
            writer.write(json.toString());
            writer.close();
        } catch (Exception e) {
            System.out.println(errorMessage);
            e.printStackTrace();
        }
    }

    /**
     * Sauvegarde les scores dans un fichier json
     */
    private void saveScores() {
        JSONObject json = new JSONObject(scores);
        saveFile(scoresFile, json, "Error while saving scores");
    }
}