package com.example.projetdumont;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Représente une question de QCM
 */
public class Question{
    private String question;
    private int reponse;
    private String[] choix;

    /**
     * Constructeur
     * @param question la question
     * @param reponse l'indice de la bonne réponse
     * @param choix les choix de réponse
     */
    public Question(String question, int reponse, String[] choix) {
        this.question = question;
        this.reponse = reponse;
        this.choix = choix;
    }

    /**
     * Charge une question depuis un objet JSON
     * @param json l'objet JSON
     * @return la question
     * @throws JSONException si le JSON est mal formé
     */
    public static Question fromJSON(JSONObject json) throws JSONException {
        String question = json.getString("question");
        JSONArray jsonChoix = json.getJSONArray("choix");
        String[] choix = new String[jsonChoix.length()];
        for (int i = 0; i < jsonChoix.length(); i++) {
            choix[i] = jsonChoix.getString(i);
        }
        int reponse = json.getInt("reponse");
        return new Question(question, reponse, choix);
    }

    /**
     * Retourne la question
     * @return la question
     */
    public String getQuestion() {
        return question;
    }

    /**
     * Retourne l'indice de la bonne réponse
     * @return l'indice de la bonne réponse
     */
    public int getReponse() { return reponse; }

    /**
     * Retourne les choix de réponse
     * @return les choix de réponse
     */
    public String[] getChoix() {
        return choix;
    }

    /**
     * Convertit la question en objet JSON
     * @return l'objet JSON
     * @throws JSONException si le JSON est mal formé
     */
    public JSONObject toJSON() throws JSONException {
        JSONObject json = new JSONObject();
        json.put("question", question);
        JSONArray json_choix = new JSONArray();
        for (String choix : choix){
            json_choix.put(choix);
        }
        json.put("choix", json_choix);
        json.put("reponse", reponse);
        return json;
    }
}
