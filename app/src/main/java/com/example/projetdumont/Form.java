package com.example.projetdumont;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import java.util.ArrayList;

/**
 * Représente un QCM
 */
public class Form {
    private String theme;
    private ArrayList<Question> questions;

    /**
     * Crée un QCM
     * @param theme le thème du QCM
     * @param questions les questions du QCM
     */
    public Form(String theme, ArrayList<Question> questions) {
        this.theme = theme;
        this.questions = questions;
    }

    /**
     * Récupère le thème du QCM
     * @return le thème du QCM
     */
    public String getTheme() {
        return theme;
    }

    /**
     * Récupère les questions du QCM
     * @return les questions du QCM
     */
    public ArrayList<Question> getQuestions() {
        return questions;
    }

    /**
     * Charge un formulaire à partir d'un objet JSON
     * @param json l'objet JSON
     * @return le QCM
     * @throws JSONException si le JSON est mal formé
     */
    public static Form fromJSON(JSONObject json) throws JSONException {
        String theme = json.getString("theme");
        JSONArray json_questions = json.getJSONArray("questions");
        ArrayList<Question> questions = new ArrayList<>();
        for (int i = 0; i < json_questions.length(); i++) {
            questions.add(Question.fromJSON(json_questions.getJSONObject(i)));
        }
        return new Form(theme, questions);
    }

    /**
     * Convertit le QCM en objet JSON
     * @return l'objet JSON
     * @throws JSONException si le JSON est mal formé
     */
    public JSONObject toJSON() throws JSONException {
        JSONObject json = new JSONObject();
        json.put("theme", theme);
        JSONArray questions = new JSONArray();
        for (Question question : this.questions) {
            questions.put(question.toJSON());
        }
        json.put("questions", questions);
        return json;
    }
}
