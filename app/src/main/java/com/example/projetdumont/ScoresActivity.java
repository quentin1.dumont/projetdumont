package com.example.projetdumont;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ListView;

import java.util.HashMap;

public class ScoresActivity extends AppCompatActivity {
    private ScoreAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scores);

        // Initialize the list
        adapter = new ScoreAdapter(this, new HashMap<>());
        ListView scoresListView = findViewById(R.id.scores);
        scoresListView.setAdapter(adapter);

        listScores();
    }

    /**
     * Ajoute les noms des QCM et leurs scores dans la liste.
     */
    private void listScores() {
        double sum = 0;

        for (String name : MainActivity.scores.keySet()) {
            String score = MainActivity.scores.get(name);
            ScoreAdapter.ScoreItem scoreItem = new ScoreAdapter.ScoreItem(name, score);
            adapter.add(scoreItem);

            // Ajout du score au calcul de la moyenne
            String[] split = score.split(" / ");
            sum += Double.parseDouble(split[0]) / Double.parseDouble(split[1]);
        }

        // Ajout de la moyenne
        if (MainActivity.scores.size() > 0) {
            double average = 20 * sum / MainActivity.scores.size();
            ScoreAdapter.ScoreItem averageItem = new ScoreAdapter.ScoreItem(getResources().getString(R.string.mean),
                    String.format(MainActivity.getLocale(this),"%.2f / 20", average));
            adapter.add(averageItem);
        }

        adapter.notifyDataSetChanged();
    }

    /**
     * Quitte l'activité.
     * @param view le bouton "OK"
     */
    public void quit(View view) {
        finish();
    }
}
