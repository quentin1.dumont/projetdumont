package com.example.projetdumont;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

public class FormActivity extends AppCompatActivity {
    private TextView question;

    private TextView questionNumber;
    private ListView listView;
    private ArrayAdapter<String> adapter;
    private int index = 0;
    private int score = 0;
    private Form form;
    private String formName;

    private ArrayList<Question> questions;
    private boolean canceled = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form);

        // Récupération du QCM
        formName = getIntent().getStringExtra("name");
        form = MainActivity.forms.get(formName);
        TextView theme = findViewById(R.id.category);
        theme.setText(form.getTheme());
        questions = (ArrayList<Question>) form.getQuestions().clone();
        Collections.shuffle(questions);

        question = findViewById(R.id.question);
        questionNumber = findViewById(R.id.question_number);
        listView = findViewById(R.id.choices);

        // Initialisation de la liste des choix de réponse
        ArrayList<String> entries = new ArrayList<>();
        adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_checked, entries);
        listView.setAdapter(adapter);
        computeQuestion();
    }

    /**
     * Gère les clics sur les boutons
     * @param view bouton cliqué
     */
    public void onClick(View view){
        if (index != form.getQuestions().size() && !canceled){
            if (view.getId() == R.id.submit) {
                // Vérification de la réponse
                if (listView.getCheckedItemPosition() == questions.get(index).getReponse()) {
                    score++;
                }
                index++;
                if (index == questions.size()) {
                    // Fin du QCM
                    end();
                } else {
                    // Question suivante
                    adapter.clear();
                    computeQuestion();
                }
            } else if (view.getId() == R.id.cancel) {
                // Annulation du QCM
                score = 0;
                canceled = true;
                end();
            }
        } else {
            if (view.getId() == R.id.submit) {
                // Affichage des scores
                Intent intent = new Intent(FormActivity.this, ScoresActivity.class);
                startActivity(intent);
                finish();
            } else if (view.getId() == R.id.cancel) {
                // Retour au menu
                finish();
            }
        }
    }

    /**
     * Affiche le score et les boutons pour revenir au choix de QCM ou voir les scores
     * Cache la liste des choix de réponse
     */
    private void end() {
        String result = score + " / " + questions.size();
        MainActivity.scores.put(formName, result);
        question.setText(result);

        questionNumber.setVisibility(View.INVISIBLE);
        listView.setVisibility(View.INVISIBLE);

        Button submit = findViewById(R.id.submit);
        submit.setText(R.string.go_to_scores);

        Button cancel = findViewById(R.id.cancel);
        cancel.setText(R.string.go_to_menu);

    }

    /**
     * Affiche la question et les choix de réponse courants
     */
    private void computeQuestion() {
        question.setText(questions.get(index).getQuestion());
        // choose locale according to the language
        questionNumber.setText(String.format(MainActivity.getLocale(this),"%s %d / %d",
                getResources().getString(R.string.question), index + 1, questions.size()));
        adapter.addAll(Arrays.asList(questions.get(index).getChoix()));
        listView.setItemChecked(listView.getCheckedItemPosition(), false);
        adapter.notifyDataSetChanged();
    }

}